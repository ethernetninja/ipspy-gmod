
# ipspy-gmod

Extremely rudimentary code to pull IP Addresses from console.log and identify the IP, ASN and ASN Name.


![alt-text](screenshot.png)

## Usage

pip3 install -r requirements.txt

Place script in same location as console.log and run
