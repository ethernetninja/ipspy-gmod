# Print ASN and name from IP list
import requests
import re
from ipwhois import IPWhois

def get_ips():
    # Input file
    input_file_path = "console.log"
    # Output file
    output_file_path = "ip_list.txt"
    regex = r'\b(?:[0-9]{1,3}\.){3}[0-9]{1,3}\b'
    
    with open(input_file_path, "r") as input_file, open(output_file_path, "w") as output_file:
        content = input_file.read()
        ip_addresses = re.findall(regex, content)
        
        if ip_addresses:
            for ip_address in ip_addresses:
                output_file.write(ip_address + "\n")
            print("Found IPs")
        else:
            print("No IP addresses found.")

def get_asn(ip):
    obj = IPWhois(ip)
    result = obj.lookup_rdap()
    asn = result['asn']
    asn_name = result['asn_description']
    return asn, asn_name

def get_ip_asn_from_file(file_path):
    asn_list = []
    asn_name_list = []
    with open(file_path, 'r') as file:
        ip_list = file.read().splitlines()
    for ip in ip_list:
        try:
            asn, asn_name = get_asn(ip)
            asn_list.append(asn)
            asn_name_list.append(asn_name)
        except Exception as e:
            print(f"Error retrieving ASN for {ip}: {str(e)}")
            asn_list.append(None)
            asn_name_list.append(None)
    return asn_list, asn_name_list

get_ips()
file_path = 'ip_list.txt'
asn_list, asn_name_list = get_ip_asn_from_file(file_path)

with open(file_path, 'r') as file:
    ip_list = file.read().splitlines()

for ip, asn, asn_name in zip(ip_list, asn_list, asn_name_list):
    print(f"IP: {ip}\t ASN: {asn}\t ASN Name: {asn_name}")
